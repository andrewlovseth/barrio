<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
		
	</section>

	<section id="main">
		<div class="wrapper">

			<div class="photo inset">
				<div class="content">
					<img src="<?php $image = get_field('inset_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
			</div>

			<div class="info">
				<div class="headline">
					<h2><?php the_field('headline'); ?></h2>
				</div>

				<div class="copy">
					<?php the_field('description'); ?>
				</div>				
			</div>

			<div class="hours">
				<div class="photo">
					<div class="content">
						<img src="<?php $image = get_field('hours_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

				<div class="details">
					<h3>Hours</h3>
					<p><?php the_field('hours', 'options'); ?></p>
				</div>
			</div>

			<div class="location">
				<div class="photo">
					<div class="content">
						<img src="<?php $image = get_field('location_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

				<div class="details">
					<h3>Location</h3>
					<p><?php the_field('location', 'options'); ?></p>
				</div>
			</div>
				
		</div>
	</section>

<?php get_footer(); ?>