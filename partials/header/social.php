<?php include(locate_template('partials/header/global-variables.php')); ?>

<div class="social">
	<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external">
		<img src="<?php echo $child_theme_path; ?>/images/facebook.svg" alt="Facebook" />
	</a>
	
	<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external">
		<img src="<?php echo $child_theme_path; ?>/images/twitter.svg" alt="Twitter" />
	</a>
	
	<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external">
		<img src="<?php echo $child_theme_path; ?>/images/instagram.svg" alt="Instagram" />
	</a>
</div>